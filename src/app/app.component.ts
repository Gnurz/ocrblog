import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'BlogAttitude';

  posts = [
    {
      title: 'Premier Post',
      // tslint:disable-next-line: max-line-length
      corps: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vel faucibus magna. Aliquam sagittis mauris ac nisl aliquet, ac semper massa egestas. Vestibulum sed mollis diam. Aenean placerat nibh eros, fringilla sodales arcu fringilla a. Proin ligula sapien, ultrices nec felis id, pellentesque ultricies diam. Suspendisse nec tellus urna. Quisque rhoncus sapien pellentesque enim pretium, at tristique lorem luctus. Pellentesque rutrum urna eget dictum tincidunt.Phasellus lacinia massa id rutrum efficitur.Nunc iaculis dui volutpat ligula sodales tristique.Aliquam malesuada diam placerat, suscipit ante eu, fermentum neque.Praesent commodo ipsum risus, et pretium eros suscipit at.Maecenas consequat vehicula diam, sed euismod tortor convallis consectetur.Aenean varius justo vel posuere rutrum.Sed nec diam quis ipsum cursus finibus ullamcorper id enim.Nullam ac volutpat sapien, at imperdiet felis.Donec hendrerit, quam quis pellentesque sodales, orci mi aliquet augue, sed lacinia mauris ex quis mi.Nulla facilisi. Nam quis purus lorem.Integer posuere est sit amet sem faucibus, nec tristique tellus posuere.Donec eget tellus vitae est efficitur tempus.Nunc ut tellus nunc.Sed id est eget augue euismod tempor a a tellus.Integer massa justo, tristique non pellentesque at, maximus ac erat.Nunc gravida faucibus vehicula.Maecenas at dapibus quam.Curabitur ultrices, nulla eget convallis porttitor, urna eros aliquet purus, suscipit egestas risus mi vel lorem.',
      created_at: Date,
      comptLike: 0
    },
    {
      title: 'Deuxième post',
      // tslint:disable-next-line: max-line-length
      corps: 'Mauris sed urna cursus, varius massa in, gravida lacus. Proin sit amet elit purus. Integer maximus ex ex, nec porttitor lorem hendrerit ac. Mauris luctus faucibus nisl, quis mollis enim aliquet ut. Ut suscipit massa sed dictum commodo. Nam vestibulum nisl consectetur mauris placerat, eu dapibus odio vestibulum. Praesent tortor nulla, interdum sed metus sollicitudin, fringilla dictum orci. In sit amet lobortis enim.',
      created_at: Date,
      comptLike: 0
    },
    {
      title: 'Troisième post',
      // tslint:disable-next-line: max-line-length
      corps: "Ca s'est de la beau litératur lolilol",
      created_at: Date,
      comptLike: 0
    }
  ];
}
