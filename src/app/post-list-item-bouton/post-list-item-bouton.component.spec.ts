import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostListItemBoutonComponent } from './post-list-item-bouton.component';

describe('PostListItemBoutonComponent', () => {
  let component: PostListItemBoutonComponent;
  let fixture: ComponentFixture<PostListItemBoutonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostListItemBoutonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostListItemBoutonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
