import { Component, Input, OnInit } from '@angular/core';
import { isNgTemplate } from '@angular/compiler';
import { stringify } from 'querystring';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {


  @Input() postTitre: string;
  @Input() postContent: string;
  @Input() loveIts: number;
  @Input() postDate: Date;
  couleur: string;


  constructor() {

  }

  getDateJour() {
    this.postDate = new Date();
    return this.postDate;
  }


  ngOnInit() {


  }

  augLike() {
    this.loveIts++;

  }


  dimLike() {
    this.loveIts--;

  }




}
