import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PostlistComponent } from './postlist/postlist.component';
import { PostListItemComponent } from './post-list-item/post-list-item.component';
import { PostListItemBoutonComponent } from './post-list-item-bouton/post-list-item-bouton.component';






@NgModule({
  declarations: [
    AppComponent,
    PostlistComponent,
    PostListItemComponent,
    PostListItemBoutonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
